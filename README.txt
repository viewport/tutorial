A starter point for any theme developer, this theme is not intended for production.

Further documentation under:

http://wiki.k15t.com/display/VPRT/Documentation